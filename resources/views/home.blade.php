@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center h2">Welcome to Expense Manager</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="card-body">
                    <a href="{{ url('transaction/create')}}"> <button type="button" class="btn btn-success float-right m-2  p-2">
                       <b> Add Transaction </b> </button> </a>
                      <a href="{{ url('account/create')}}"> <button type="button" class="btn btn-success float-left m-2  p-2">
                       <b> Add New Account </b> </button> </a>
                    </div>
                </div>
                <h3 style="text-align:center; color:chocolate">{{Session::get('Message')}}</h3> 

            @if($accounts)
                @foreach($accounts as $account)
                  <div class="container">
                    <div class="row justify-content-center">
                            <div class="card">
                                <div class="card-header text-center h3">{{$account->name}}
                                    @if (\Illuminate\Support\Str::contains($account->name,['Default Account']))
                                        
                                    @else
                                    <a href="{{ url('account/'. $account->id . '/edit')}}">  <button type="button" class="btn btn-primary ">
                                        <b> EDIT  </b> </button> </a>
                                    @endif

                                </div>
                                    <div class="card-body">
                                  @if (session('status'))
                                      <div class="" role="alert">
                                          {{-- {{ session('status') }} --}}
                                      </div>
                                  @endif
                                  <div class="card-body">
                                  <a href="{{ url('income/'.$account->id.'')}}"><button type="button" class="btn btn-success ">
                                          <b> Income  </b> </button> </a>
                                    <a href="{{ url('expense/'.$account->id.'')}}"><button type="button" class="btn btn-success">
                                          <b> Expense </b> </button> </a>
                                    <a href="{{ url('transfer/'.$account->id.'')}}"><button type="button" class="btn btn-success">
                                          <b> Transfer </b> </button> </a>
                                    <a href="{{ url('transaction/'.$account->id.'')}}">  <button type="button" class="btn btn-success ">
                                          <b> Transactions </b> </button> </a><br><br>
                                    <a href="{{ url('/adduser')}}">  <button type="button" class="btn btn-success col-sm-12 center ">
                                          <b> Add User </b> </button> </a>
                                      <br>
                                      <br>
                                  </div>
                                </div>
                               </div>
                          </div>
                      </div><br> <br>  
                @endforeach
            @endif
                  </div>
            </div>
      </div>
</div>

@endsection
