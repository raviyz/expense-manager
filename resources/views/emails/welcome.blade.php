@component('mail::message')
# Welcome to Expense Manager App

Thanks for signing up. **We Really Appreciate** it. Let us _know if we can_ do more to please you !

@component('mail::button', ['url' => 'http://localhost/Expense_Manager/'])
View Dashboard
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
