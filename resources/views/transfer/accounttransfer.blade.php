<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Account Transfers</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container"><br><br>
        <h2 class="text-center">Transfers</h2>
     <table class="table table-light table-hover"> 
        <thead>
          <tr>
            <th>ID</th>
            <th>Account Name</th>
            <th>Category</th>
            <th>Amount</th>
            <th>Detail</th>
            <th>Created Date</th>
            <th>Updated Date</th>
          </tr>
        </thead>
        @foreach ($transfers as $transfer)
        <tbody>
          <tr>
            <td>{{$transfer->id}}</td>
            <td>{{$account_name->accounts->name}}</td>
            <td>{{$transfer->transaction_type}}</td>
            <td>{{$transfer->amount}}</td>
            <td>{{$transfer->detail}}</td>
            <td>{{$transfer->created_at->diffForHumans()}}</td>
            <td>{{$transfer->updated_at->diffForHumans()}}</td>
          </tr>
          @endforeach
        </tbody>
    </table>
    @if(!$total)
    <h3>There is no transaction</h3>
    <td><a href="{{ url('home')}}"><button type="button" class="btn btn-danger col-sm-12 "><b> Back to Home </b></button></a></td>      
    @else
    <h3 style="text-align:center">Total :{{$total}}</h3>
    <td><a href="{{ url('home')}}"><button type="button" class="btn btn-danger col-sm-12 "><b> Back to Home </b></button></a></td>      
    @endif
</body>
</html>



