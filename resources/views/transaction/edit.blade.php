<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>
            <br><br>
            <div class="container">
        <h1 class="text-center">EDIT TRANSACTION</h1>
    
        {!! Form::model($data,['method'=>'PATCH', 'action'=>['TransactionController@update',$data->id],]) !!}
    
        <div class="form-group">
            {!! Form::label('name', 'Choose Account:') !!}
           
            {!! Form::text('account_id',$account->name , ['class'=>'form-control','placeholder'=>'Enter Account Name','required','disabled']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('name', 'Choose Category:') !!}<br>
            {!! Form::select('transaction_type', ['Income' => 'Income', 'Expense' => 'Expense','Transfer' => 'Transfer'],null, ['onchange'=>'transfer()', 'id'=>'sDropdown']) !!}
        </div> 

        <div class="form-group" id="to_user_div" style="display:none">
        
            {!! Form::label('to_user_id_1', 'Select User:') !!}<br>

            @if (!$adduser ?? '')
                 {!! Form::label('to_user_id', 'There is No User(s)') !!}
            @else
                 {!! Form::select('to_user_id', $adduser , null, ['class'=>'form-control']) !!}
            @endif
            
        </div>
        
        <div class="form-group">
            {!! Form::label('name', 'Amount:') !!}
            {!! Form::text('amount', null, ['class'=>'form-control','placeholder'=>'Enter Amount','required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('name', 'Note:') !!}
            {!! Form::text('detail', null, ['class'=>'form-control','placeholder'=>'Enter Note/Detail']) !!}
        </div><br>

        <div class="form-group">
            {!! Form::submit('Update Transaction', ['class'=>'btn btn-success col-sm-12']) !!}&nbsp;
        
        {!! Form::close() !!}

    
        {!! Form::open(['method'=>'DELETE', 'action'=>['TransactionController@destroy', $data->id]]) !!}

        
            {!! Form::submit('Delete Transaction', ['class'=>'btn btn-danger col-sm-12']) !!}
        </div>
        <td><a href="{{ url('transaction/'.$account->id.'')}}"><button type="button" class="btn btn-primary col-sm-12 "><b>  Cancel  </b></button></a></td>

        {!! Form::close() !!}
        
        </div>
        <script>
            let sDropdown = document.getElementById("sDropdown");
            let to_user_div = document.getElementById("to_user_div");
            console.log(sDropdown);
            
            function transfer(){

                var trans = document.getElementById("sDropdown");
                
                if(trans.value === "Transfer" && to_user_div.style.display == "none")
                {  
                    to_user_div.style.display = "block";
                }
                else if(to_user_div.style.display == "block")
                {
                    to_user_div.style.display = "none";
                }
            }
        </script>

</body>
</html>