<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Account Transactions</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</head>
<body>
    <div class="container"><br><br>
        <h3 style="text-align:center; color:red" >{{Session::get('Message')}}</h3> 
        <h2 class="text-center">Transactions</h2>
    <table class="table table-light table-hover">
        <thead>
          <tr>
            <th>ID</th>
            <th>Account Name</th>
            <th>Category</th>
            <th>Amount</th>
            <th>Detail</th>
            <th>Created Date</th>
            <th>Updated Date</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
            @if ($transact)
                @foreach ($transact as $trans)
        <tr>
          <td>{{$trans->id}}</td>
          <td>{{($account_name->accounts->name) }}</td>
          <td>{{$trans->transaction_type}}</td>
          <td>{{$trans->amount}}</td>
          <td>{{$trans->detail}}</td>
          <td>{{$trans->created_at->diffForHumans()}}</td>
          <td>{{$trans->updated_at->diffForHumans()}}</td>
          <td><a href="{{ url('transaction/'.$trans->id.'/edit')}}"><button type="button" class="btn btn-success "><b>  EDIT  </b></button></a></td>
        </tr>
                @endforeach
            @endif
        </tbody>
    </table>
    <div>
      @if(!$totalall && !$totalexpense && !$totaltransfer && !$total_income)
          <h3>There is no transaction</h3>
          <td><a href="{{ url('home')}}"><button type="button" class="btn btn-danger col-sm-12 "><b> Back to Home </b></button></a></td>
      @else
        <b><h2 style="text-align:center">Total : {{$totalall}}</h2></b>
            <h4>Expense  : {{$totalexpense}}</h4>
            <h4>Transfer &nbsp;: {{$totaltransfer}}</h4>
            <h4>Income  &nbsp;&nbsp;: {{$total_income > 0 ? $total_income: $total_income.'(You have no money)'}}</h4> 
            <td><a href="{{ url('home')}}"><button type="button" class="btn btn-danger col-sm-12 "><b> Back to Home </b></button></a></td>
      @endif
    </div>
</body>
</html>