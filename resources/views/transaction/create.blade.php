<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create Transaction</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
    <body>
            <br><br>
            <div class="container">
        <h1 class="text-center">ADD TRANSACTION</h1>
    
        {!! Form::open(['method'=>'POST', 'action'=>'TransactionController@store']) !!}
    
        <div class="form-group">
            {!! Form::label('account', 'Select Account:') !!}<br>
            {!! Form::select('account_id', $account , null, ['class'=>'form-control','required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('category', 'Select Category:') !!}<br>
            {!! Form::select('transaction_type', ['Income' => 'Income', 'Expense' => 'Expense','Transfer' => 'Transfer'],null, ['onchange'=>'transfer()', 'id'=>'sDropdown']) !!}
        </div> 
       
        <div class="form-group" id="to_user_div">
        
            {!! Form::label('to_user_id_1', 'Select User:(For Transfer)') !!}<br>

            @if (!$adduser)
                 {!! Form::label('to_user_id','There is no user add for TRANSFER') !!}
            @else
                 {!! Form::select('to_user_id',['' => 'Choose User']+ $adduser  , null, ['class'=>'form-control','id'=>'adduserid','disabled','required']) !!}
            @endif
            
        </div>

        <div class="form-group">
            {!! Form::label('amount', 'Enter Amount:') !!}
            {!! Form::text('amount', null, ['class'=>'form-control','placeholder'=>' Amount','required','numeric']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('note', 'Enter Any Note:') !!}
            {!! Form::text('detail', null, ['class'=>'form-control','placeholder'=>'Enter Note/Detail']) !!}
        </div>

        <div class="form-group ">
            {!! Form::submit('Add Transaction', ['class'=>'btn btn-success col-sm-12']) !!}  
        </div>

            <td><a href="{{ url('home')}}"><button type="button" class="btn btn-primary col-sm-12 "><b> Cancel </b></button></a></td><br/>
            {!! Form::close() !!}
            <br>

        <div class="form-group">
        <td><a href="{{ url('home')}}"><button type="button" class="btn btn-danger col-sm-12 "><b> Back to Home </b></button></a></td>
        </div>
            </div>
                <script>
                    let sDropdown = document.getElementById("sDropdown");
                    let to_user_div = document.getElementById("to_user_div");
                    let adduserid = document.getElementById("adduserid");
                    console.log(sDropdown);
                    
                    function transfer(){

                        var trans = document.getElementById("sDropdown");
                        
                        if(trans.value === "Transfer")
                        {  
                            adduserid.disabled = false;
                        }
                        else
                        {
                            adduserid.disabled = true;
                        }
                    }
                </script>
    </body>
</html>

