<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Transaction;
use App\User;

class Account extends Model
{
    protected $table = "accounts";

    protected $fillable = 
    [ 
        'name','owner_id','created_at','updated_at',
    ];

    

}
