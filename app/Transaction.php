<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Account;

class Transaction extends Model
{
    protected $table = "transactions";

    protected $fillable = [ 
        'account_id','transaction_type','user_id','to_user_id','amount','detail','created_at','updated_at',
    ];

    public function accounts()
    {
            return $this->belongsTo('App\Account','account_id'); 
    }
    

}
