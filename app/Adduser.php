<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Adduser extends Model
{
    //
    protected $table = "add_users";

    protected $fillable = [ 
        'owner_id','user_id','created_at','updated_at',
    ];

    public function users()
    {
       return $this->belongsTo('App\User','user_id');
    }

    
}
  