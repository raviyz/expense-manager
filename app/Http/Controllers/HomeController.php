<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Mail;
use App\Mail\NewUserWelcome;
use App\Account;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users    =  User::where('id',auth()->user()->id)->get();
        $accounts =  Account::where('owner_id',auth()->user()->id)->get();
        
             return view('home',compact(['users','accounts']));
    }
}
