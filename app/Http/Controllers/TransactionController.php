<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Transaction;
use Auth;
use App\User;
use App\Account;
use App\Adduser;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $account = Account::where('owner_id',['owner_id'=>auth()->user()->id])->pluck('name','id')->all();
        $adduser = Adduser::with('users')->where('owner_id',auth()->user()->id)->pluck('user_id','user_id')->all();  
            // dd($adduser);

        $adduser = Adduser::with('users')->first();
        // dd($adduser->users->email);

            $user = User::pluck('email','id');
            // dd($user->all());

                  return view('transaction.create', compact('account','adduser'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Transaction::create(
                $request->all() +  ['user_id'=>auth()->user()->id]);
                session()->flash('Message', 'Transaction Added Successfully!');

                return redirect('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transact       = Transaction::where('account_id',$id)->get();
        $totalall       = Transaction::where('account_id',$id)->sum('amount');
        $totalexpense   = Transaction::where('transaction_type','=','Expense')->where('account_id',$id)->sum('amount');
        $totalincome    = Transaction::where('transaction_type','=','Income')->where('account_id',$id)->sum('amount');
        $totaltransfer  = Transaction::where('transaction_type','=','Transfer')->where('account_id',$id)->sum('amount'); 
        $total_income = ($totalincome)-($totalexpense);
        $account_name = Transaction::with('accounts')->where('account_id',$id)->first();
            // dd($account_name->toArray());
                return view('transaction.show',compact('transact','totalexpense','totalincome','totaltransfer','totalall','account_name','total_income'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data       = Transaction::findOrFail($id);
        $account_id = $data->account_id;
        $account    = Account::findOrFail($account_id);
        $data->with(['account_details' => $account]);
        $adduser = User::where('id','!=',auth()->user()->id)->pluck('email','id')->all(); 

                return view('transaction.edit', compact('data','account','adduser'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input       = $request->all();
        $transaction = Transaction::findOrFail($id);
        $account     = Account::findOrFail($transaction->account_id);
        $transaction->update($input);
        session()->flash('Message', 'Transaction Updated Successfully!');

                return redirect( url ('transaction/'.$account->id.''));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $data         = Transaction::findOrFail($id);
        $account_data = Account::findOrFail($data->account_id);
        $data->delete();
        session()->flash('Message', 'Transaction Deleted Successfully!');

                return redirect( url('transaction/'.$account_data->id.'') );
    }
}
